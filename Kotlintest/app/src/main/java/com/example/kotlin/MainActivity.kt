package com.example.kotlin

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.setContentView(R.layout.activity_main)

        //
        val myIntent = Intent(this,Kotlintest2::class.java)
        val recive = intent
        txt_recive.setText(recive.getStringExtra("sendmessage"))
        //
        val button = findViewById(R.id.btn_btn1) as Button;
        button.setOnClickListener {
            if(myIntent.resolveActivity(packageManager) != null){
                startActivity(myIntent)
            }else {
                Log.d("StartActivity", "Fail!!")
            }
        }


    }
}
