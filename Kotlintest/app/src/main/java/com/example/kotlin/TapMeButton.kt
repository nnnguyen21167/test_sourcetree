package com.example.kotlin

import android.content.IntentSender
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.os.PersistableBundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_tap_me_button.*

class TapMeButton : AppCompatActivity() {

    internal lateinit var tapMeButton: Button
    internal lateinit var gameScore: TextView
    internal lateinit var timeRemaining: TextView

    internal var score = 0;

    internal lateinit var countDownTimer: CountDownTimer
    internal var gameStarted = false
    internal var initialCountDown: Long = 5000
    internal var countDownInterval: Long = 1000
    internal val initialTimeLeft = initialCountDown/1000
    internal var timeLeftOnTimeInit: Long = 5000

    private val TAG = MainActivity::class.java.simpleName

    companion object {
        private val SCORE_KEY = "SCORE_KEY"
        private val TIME_LEFT_KEY = "TIME_LEFT_KEY"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tap_me_button)
        Log.d(TAG,"onCreate: Score = $score")
        addcontrol()
        resetGame()

        tapMeButton.setOnClickListener { v ->
            gameScore.setText(score.toString())
            incrementScore()
        }
    }

    override fun onSaveInstanceState(outState: Bundle/*, outPersistentState: PersistableBundle?*/) {
        super.onSaveInstanceState(outState)
        outState.putInt(SCORE_KEY,score)
        outState.putLong(TIME_LEFT_KEY,timeLeftOnTimeInit)
        countDownTimer.cancel()
        Log.d(TAG,"Saving score: ${this.score} and saving time: ${this.timeLeftOnTimeInit}")
    }

    private fun resetGame(){
        score = 0
        gameScore.text = getString(R.string.score,score.toString())
        countDownTimer = object: CountDownTimer(initialCountDown,countDownInterval)
        {
            override fun onTick(milisUntilFinished: Long) {
                val timeLeft = milisUntilFinished/1000
                timeLeftOnTimeInit = milisUntilFinished
                    timeRemaining.text =getString(R.string.time_remaining, timeLeft.toString())
            }
            override fun onFinish() {
                    timeRemaining.text = getString(R.string.time_remaining, initialTimeLeft.toString())
                    Toast.makeText(getApplicationContext(),"điểm số: " + score,Toast.LENGTH_LONG).show()
                    score = 0
                    gameStarted = false
                    tapMeButton.text = getString(R.string.btn_tapme)
            }
        }
    }

    private fun incrementScore() {
        if (!gameStarted){
            countDownTimer.start()
            gameStarted = true
        }
        tapMeButton.text = getString(R.string.btn_hitme)
        score ++
        val newScore = getString(R.string.score, score.toString())
        gameScore.text = newScore
    }

    fun addcontrol(){
        tapMeButton = findViewById<Button>(R.id.btn_tapme)
        gameScore = findViewById(R.id.txt_score)
        timeRemaining = findViewById(R.id.txt_ctime)
        timeRemaining.text = getString(R.string.time_remaining, initialTimeLeft.toString())
        gameScore.text = getString(R.string.score,"0")
    }
}
