package com.hfood.hfood.ui.tabs_main;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import com.hfood.hfood.R;

public class tabs_main extends AppCompatActivity {

    private SectionPageAdapter sectionPageAdapter;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private int[] tabIcons = {
            R.drawable.menu,
            R.drawable.purchase,
            R.drawable.news,
            R.drawable.plus
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabs_main);
        mapping();
        sectionPageAdapter = new SectionPageAdapter(getSupportFragmentManager());
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcon();
    }
    //ánh xạ id
    public void mapping() {
        viewPager = (ViewPager) findViewById(R.id.container);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
    }
    //hàm add fragment vào tabs
    private void setupViewPager(ViewPager viewPager) {
        sectionPageAdapter = new SectionPageAdapter(getSupportFragmentManager());
        sectionPageAdapter.addFragment(new fragment_menu(), "Menu");
        sectionPageAdapter.addFragment(new fragment_order(), "Order");
        sectionPageAdapter.addFragment(new fragment_info(), "Info");
        //sectionPageAdapter.addFragment(new fragment_addproduct(), "Add");
        viewPager.setOffscreenPageLimit(3);
        viewPager.setAdapter(sectionPageAdapter);
    }

    private void setupTabIcon() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
       // tabLayout.getTabAt(3).setIcon(tabIcons[3]);
    }
    //

}
