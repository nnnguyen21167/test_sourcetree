package com.hfood.hfood.ui;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.hfood.hfood.R;

public class Buyer_SignupActivity extends AppCompatActivity {

    private EditText txtaccount, txtpassword;
    private Button btnsignup,btnforgetpwd;
    private FirebaseAuth auth;
    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buyer__signup);

        auth =FirebaseAuth.getInstance();
        init();
        btnsignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String account = txtaccount.getText().toString().trim();
                String password = txtpassword.getText().toString().trim();

                if(TextUtils.isEmpty(account)){
                    progressBar.setVisibility(View.GONE);
                    txtaccount.setError("Vui lòng nhập tài khoản!!!");
                    return;
                }
                if(TextUtils.isEmpty(password)){
                    progressBar.setVisibility(View.GONE);
                    txtpassword.setError("Vui lòng nhập mật khẩu");
                    return;

                }
                if(password.length()<6){
                    Toast.makeText(getApplicationContext(),"Nhập mật khẩu lớn hơn 6 ký tự",Toast.LENGTH_SHORT);
                    return;

                }
                progressBar.setVisibility(View.VISIBLE);

                //Tạo mới người dùng
                auth.createUserWithEmailAndPassword(account,password).addOnCompleteListener(Buyer_SignupActivity.this,
                        new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                Toast.makeText(Buyer_SignupActivity.this, "Tạo tài khoản thành công:" + task.isSuccessful(), Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.GONE);
                                if (!task.isSuccessful()) {
                                    Toast.makeText(Buyer_SignupActivity.this, "Không tạo được tài khoản" + task.getException(),
                                            Toast.LENGTH_SHORT).show();
                                } else {
                                    startActivity(new Intent(Buyer_SignupActivity.this, Main2Activity.class));
                                    finish();
                                }
                            }
                        });
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        progressBar.setVisibility(View.GONE);
    }
    public void init(){
        txtaccount = (EditText)findViewById(R.id.txt_account);
        txtpassword = (EditText)findViewById(R.id.txt_password);
        btnsignup = (Button)findViewById(R.id.btn_signup);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
    }
}
