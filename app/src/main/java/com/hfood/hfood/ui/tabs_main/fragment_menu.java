package com.hfood.hfood.ui.tabs_main;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hfood.hfood.R;
import com.hfood.hfood.common.FirebaseConfig;
import com.hfood.hfood.menu_list.ListAdapter;
import com.hfood.hfood.model.Product;

import java.util.ArrayList;

public class fragment_menu extends Fragment{

    DatabaseReference readAll;
    FirebaseDatabase dbFireBase;
    FirebaseAuth mAuth;

    ArrayList<Product> list;
    ListAdapter adapter;
    ListView listView;
    Product product;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_menu, container, false);

        readAll = dbFireBase.getInstance().getReference("product");
        mAuth = FirebaseAuth.getInstance();
        listView = (ListView)view.findViewById(R.id.lsv_sanpham);
        list = new ArrayList<Product>();

        readAll.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot ds: dataSnapshot.getChildren()){
                    Product product2 = ds.getValue(Product.class);
                    product=product2;
                    list.add(product);
                }
                adapter = new ListAdapter(getContext(),R.layout.activity_list_item_menu, list);
                adapter.notifyDataSetChanged();
                listView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return view;
    }
}
