package com.hfood.hfood.common;

public class FirebaseConfig {
    public static final String FIREBASE_URL = "https://quanlyshipper-29bfb.firebaseio.com/";
    public static final String USERS_CHILD = "users";
    public static final String SHIPPER_LOCATION_CHILD = "shipper_location";
    public static final String DIRECTION_URL_API = "https://maps.googleapis.com/maps/api/directions/json?";
    public static final String ORDER_CHILD = "orders";
    public static final String SHIPPER_PRESENCES = "presences";
    public static final String FIREBASE_STORAGE_URL = "gs://quanlyshipper-29bfb.appspot.com";
    public static final String TOPIC_USER = "topic%users";
    public static final String CUSTOMER = "Customer";
}
