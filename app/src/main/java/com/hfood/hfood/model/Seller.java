package com.hfood.hfood.model;

public class Seller {
    private String name; // Tên người dùng
    private String storename;
    private String phone; // số điện thoại người dùng
    private String url; // link hình ảnh cá nhân
    private String date; // ngày tạo tài khoản
    private String address; // địa chỉ
    private String sellerID; // mã người dùng
    private String status; // trạng thái người dùng
    private String intro; // gioi thieu seller
    private double lat; // vi tri
    private double lng; // vi tri

    public Seller(){
    }

    public Seller(String name, String storename,String phone,String url,String date, String address,String sellerID,String status,String intro,double lat,double lng){
        this.name = name;
        this.storename = storename;
        this.phone = phone;
        this.url = url;
        this.date = date;
        this.address = address;
        this.sellerID = sellerID;
        this.status = status;
        this.intro = intro;
        this.lat = lat;
        this.lng = lng;
    }
}


