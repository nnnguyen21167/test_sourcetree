package com.hfood.hfood.model;

public class Product {
    String name;
    String typedetail;
    String date;
    double price;
    double saleprice;
    String status;
    String img;
    String description;
    public Product(String date,String description , String img,String name, double price, double saleprice, String status,String typedetail ) {
        this.name = name;
        this.typedetail = typedetail;
        this.date = date;
        this.price = price;
        this.saleprice = saleprice;
        this.status = status;
        this.img = img;
        this.description = description;
    }

    public Product (){};

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTypedetail() {
        return typedetail;
    }

    public void setTypedetail(String typedetail) {
        this.typedetail = typedetail;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getSaleprice() {
        return saleprice;
    }

    public void setSaleprice(double saleprice) {
        this.saleprice = saleprice;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
