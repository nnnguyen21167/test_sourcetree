package com.hfood.hfood.menu_list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hfood.hfood.R;
import com.hfood.hfood.model.Product;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ListAdapter extends ArrayAdapter<Product> {
    public ListAdapter(Context context, int resource, List<Product> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view =  inflater.inflate(R.layout.activity_list_item_menu, null);
        }
        Product p = getItem(position);
        if (p != null) {
            // Anh xa + Gan gia tri
            TextView txt_tensp = (TextView) view.findViewById(R.id.txt_tensp);
            TextView txt_giasp = (TextView) view.findViewById(R.id.txt_giasp);
            TextView txt_motasp = (TextView) view.findViewById(R.id.txt_motasp);
            ImageView imageView = (ImageView)view.findViewById(R.id.imgv_sp);

            txt_tensp.setText(p.getName());
            txt_giasp.setText(String.valueOf(p.getPrice()));
            txt_motasp.setText(p.getDescription());
            Picasso.get().load(p.getImg()).into(imageView);
        }
        return view;
    }
}
